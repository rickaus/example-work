/**************************************************
 * CallStatisticsBolt
 *  - total number of calls 
 *  - total call minutes
 *  - average minutes per users
 *  - most frequent users by minutes 
 *  - most frequent users by number of calls 
 **************************************************/

package Bolts;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Iterator;
import java.util.Map;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import backtype.storm.Config;
import backtype.storm.Constants;
import backtype.storm.task.OutputCollector;
import backtype.storm.task.TopologyContext;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.topology.base.BaseRichBolt;
import backtype.storm.tuple.Tuple;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Values;

public class CallStatisticsBolt extends BaseRichBolt {

	private final boolean _DEBUG = false;   /* show debug output */
	
	private OutputCollector _collector;
	private final String[] _statisticsFields = {"Analytic_Result_Map"};
	private final String[] _cdrReduceFields = 
		{"CDRType", "CallingNumber", "CalledNumber", "Location","CallDuration", "AnswerTime", "CauseForTermination"};
	
    // variables for printing incoming stream speed
    private Calendar _LAST_PRINT_SPEED_DATE;			/* store the date of last time printing speed */
	private int _STATISTICS_INTERVAL = 10;			/* send new statistics every X seconds */
	
	// variables for statistics aggregating
	private long _TOTAL_NUM_CALLS;					/* number of calls (cdr) */
	private long _TOTAL_MINUTES =0;					/* number of minutes on the system */
	private long _TOTAL_CALLS_DROPPED =0;				/* number of calls with termination code "04" */
	private final String _DROPPED_CODE = "04";
	
	// variables for most frequent users by minutes (approximate)
	private TopK _TOP_USERS_BY_MINUTES;				/* user and number of minutes*/
	
	// variables for most frequent users by number of calls (approximate)
	private TopK _TOP_USERS_BY_CALLS;				/* users and number of calls */
	
	
	//distinct callers (exact) 
	private ArrayList<String> _DISTINCT_USERS_EXACT;
	
	// top users by number of calls (exact) - test accuracy of approximate
	private HashMap<String,Integer> _ALL_USERS_BY_CALLS_EXACT;
	
	//most often called area codes
	private TopK _TOP_CALLED_AREA_CODE;


        /*******************************
	 *  Constructor 
	 *******************************/	
	public CallStatisticsBolt (int flushIntervalSeconds){
		_STATISTICS_INTERVAL = flushIntervalSeconds;
		_LAST_PRINT_SPEED_DATE =  Calendar.getInstance();
		_TOP_USERS_BY_MINUTES = new TopK(100);
		_TOP_USERS_BY_CALLS = new TopK(100);
		_DISTINCT_USERS_EXACT = new ArrayList<String>();
		_ALL_USERS_BY_CALLS_EXACT = new HashMap<String,Integer>();
		_TOP_CALLED_AREA_CODE = new TopK(100);
	}
	
	@Override
	public void prepare(Map stormConf, TopologyContext context, OutputCollector collector) {
		_collector = collector;
		_TOTAL_MINUTES =0;
	}

	@Override
	public void execute(Tuple input) {
		
		/**************************************************
		 * send fresh statistics downstream
		 **************************************************/
		if(isTickTuple(input)){
			Calendar now = Calendar.getInstance();
			long nowInSeconds = now.getTimeInMillis()/1000;
			long lastPrintInSeconds = _LAST_PRINT_SPEED_DATE.getTimeInMillis()/1000;
			long timeDifferenceInSeconds = nowInSeconds - lastPrintInSeconds;		
			
			HashMap<String, Object> newResults = new HashMap<String, Object>();
			newResults.put("Total_Minutes", Long.valueOf(_TOTAL_MINUTES));  
			newResults.put("Total_Calls", Long.valueOf(_TOTAL_NUM_CALLS)); 
			newResults.put("Total_Calls_Dropped", Long.valueOf(_TOTAL_CALLS_DROPPED));
			//newResults.put("Distinct_Users_Exact", Long.valueOf(_DISTINCT_USERS_EXACT.size()));  /* size() returns int !!*/
			newResults.put("Top_Users_By_Minutes_Aprox", _TOP_USERS_BY_MINUTES.getTopK());
			newResults.put("Top_Called_Area_Code_Aprox", _TOP_CALLED_AREA_CODE.getTopK());
			
			// send statistics to merge bolt
			_collector.emit(new Values(newResults));
			
			//reset last print date
			_LAST_PRINT_SPEED_DATE =  Calendar.getInstance();
			_collector.ack(input);
			return;
		}
		
		
		/**************************************************
		 * else continue aggregating inputs
		 **************************************************/		
		else{
			// send tuple to CDR stream for SPS Rule Evaluator
			_collector.emit("CDR_STREAM", input.getValues());
			
			_TOTAL_NUM_CALLS++;			
			
			/**************************************************
			 * get values from input tuple
			 **************************************************/		
			int callDuration = Math.round(Float.parseFloat(input.getStringByField("CallDuration")));   /* not good ***********/
			String callingNumber = input.getStringByField("CallingNumber");
			String areaCode = input.getStringByField("CalledNumber").substring(1, 4);
			String causeForTermination = input.getStringByField("CauseForTermination");
			
			if (causeForTermination.equalsIgnoreCase(_DROPPED_CODE)){
				_TOTAL_CALLS_DROPPED++;
			}
				
			
			/**************************************************
			 * aggregate new input tuple...
			 **************************************************/	
			callDuration = callDuration /2;
			_TOTAL_MINUTES += callDuration/60;
			_TOP_USERS_BY_MINUTES.add(callingNumber, callDuration/60);
			_TOP_USERS_BY_CALLS.add(callingNumber, 1);
			_TOP_CALLED_AREA_CODE.add(areaCode, 1);
			
			// this test shows that the top_users_by_calls statistic is not that useful 
			// most users will make 1 to 2 calls
			/*			
			if(!_DISTINCT_USERS_EXACT.contains(callingNumber)) _DISTINCT_USERS_EXACT.add(callingNumber);			
			Integer numCalls = _ALL_USERS_BY_CALLS_EXACT.get(callingNumber);
			if (numCalls == null){
				_ALL_USERS_BY_CALLS_EXACT.put(callingNumber, Integer.valueOf(1));
			}
			else{
				_ALL_USERS_BY_CALLS_EXACT.put(callingNumber, Integer.valueOf(numCalls.intValue() + 1));
			}
			*/	
			
			_collector.ack(input);
			
		}
	}

	@Override
	public void declareOutputFields(OutputFieldsDeclarer declarer) {
		declarer.declare(new Fields(_statisticsFields));
		declarer.declareStream("CDR_STREAM", false, new Fields(_cdrReduceFields));

		
	}
	
    // set alarm clock to send Statistics
	@Override
	public Map<String,Object> getComponentConfiguration(){
		// look at http://www.michael-noll.com/blog/2013/01/18/implementing-real-time-trending-topics-in-storm/
		Config conf = new Config();
		conf.put(Config.TOPOLOGY_TICK_TUPLE_FREQ_SECS, _STATISTICS_INTERVAL); 
		return conf;
	}
	// checks if alarm is received
	private static boolean isTickTuple(Tuple tuple) {			
        return tuple.getSourceComponent().equals(Constants.SYSTEM_COMPONENT_ID)
            && tuple.getSourceStreamId().equals(Constants.SYSTEM_TICK_STREAM_ID);
    }
	
}

/* Re-usable class to track Top K of any kind of analytic  */
class TopK implements java.io.Serializable{
	private int _numElements = 0;
	private ConcurrentHashMap<String,Integer> _topK;
	private int _k;
	public  TopK(int k){
		_topK = new ConcurrentHashMap<String,Integer>();	
		_numElements = 0;
		_k = k;
	}

	public void add (String callingNumber, int callMinutes){
		_numElements +=1;
		Integer totalMinutes = _topK.get(callingNumber);
		
		if (totalMinutes != null){
			_topK.put(callingNumber, Integer.valueOf(totalMinutes.intValue() + callMinutes));
		}
		else if(_topK.size() < _k){
			_topK.put(callingNumber, Integer.valueOf(callMinutes));
		}
		else{
			Map.Entry<String,Integer> min = getMin();
			_topK.put(callingNumber,min.getValue() );
			_topK.remove(min.getKey());
		}	
	}
	
	public ConcurrentHashMap<String,Integer> getTopK(){
		return _topK;
	}	
	
	/*
	public Map<String,Integer> getTopK(int i){
		return subset of _topK;
	}
	*/
	

	public void setK (int k){
		_k =k;
	}
	
	public void clear(){
		_numElements =0;
		_topK.clear();
	}
	
	// O(n) - we could be better than this
	private Map.Entry<String,Integer> getMin(){
		Map.Entry<String,Integer> min = null; 
		Iterator <Map.Entry<String,Integer>>it = _topK.entrySet().iterator();
		if (it.hasNext()) min = it.next();
		
		Map.Entry<String,Integer> entry;
		while (it.hasNext()){
			entry = it.next();
			if(entry.getValue().intValue() < min.getValue().intValue()){
				min = entry;
			}			
		}
		return min;
	}
}

