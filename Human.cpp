/* 
 * C++
 *
 * Human.cpp
 *
 * Models Human behaviour in an epidemic model (disease infection model)
 * Humans have their own intelligence to move, attack, reproduce, and fear.
 *
 * Uses Mersenne Twister random number generator.
 */

#include "Human.h"
#include "Zombie.h"
#include "ZWException.h"
#include "Exposed.h"
#include "flags.h"
#include "Global_Const.h"



unsigned long Human::_humancount = 0;

/************************************
 * HUMAN CONSTRUCTOR
 * gender {0=female} {1 = male}
 ***********************************/



Human::Human(int age, int gender, int x, int y, World* world, bool hasGun, int gunKillRate):Entity(age,gender,x,y,world,_humancount)
{    
	_has_gun = hasGun;
    _exposed = false;
    _gunKillRate = gunKillRate;                            /* change to 20 for to model headshot needed to kill */
    _pregnantTimeLeft = 0;                                 /* ==0 not pregnant; >0 is pregnant */
	_isPregnant = false;
	_birthRemaining = 0;			                       /* default value for male*/
	if (gender == 0) _birthRemaining = HUMAN_MAX_BIRTH;	   /* if female, set using global_const */
	_lifespan = HUMAN_LIFESPAN_MEDIAN * TICKS_PER_YEAR;    /* global_const */

   if(world != NULL)
	{
		#pragma omp atomic
		_humancount++;
	}
}


/************************************
 *  METHODS
 ***********************************/

/* serialize human objects to send between cores using MPI*/
Serialized_data Human::serialize()
{
	Serialized_data s;
	s = Entity::serialize();
	s.entity_type = this->type();
	s.entity_specific._has_gun = _has_gun;

	return s;

}
Entity_Type Human::type()
{
	if(_exposed) return EXPOSED;
	return HUMAN;
}

/* main method to look around and determine action */

int Human::activate_action()
{
	int x =0;
	int y=0;
	ACTION result = look(&x, &y);
	if(_gender==0){
		char text[50] = "";
		sprintf(text, "H0,%d,%d\n", _x, _y);
		_world->writeToFile(text);
	}
	else if(_gender==1){
		char text[50] = "";
		sprintf(text, "H1,%d,%d\n", _x, _y);
		_world->writeToFile(text);
	}
	switch(result)
	{
        case MOVE:
        	LOG(printf("[Human::activate_action()] H(%d,%d) move to=%d,%d\n",_x,_y,x,y));
            move(x,y);
            break;

        case ATTACK:
        	LOG(printf("[Human::activate_action()] H(%d,%d)attack=%d,%d\n",_x,_y,x,y));
            attack(x,y);
            break;
        
        case STAY:
        	LOG(printf("[Human::activate_action()] H(%d,%d) stay at=%d,%d\n",_x,_y,x,y));
            break;
        case REPRODUCE:
            LOG(printf("[Human::activate_action()] H(%d,%d) repro with=%d,%d\n",_x,_y,x,y));
            reproduce(x,y);
            break;
	}
    // if female, decrement pregnancyTimeLeft
    if(_gender == 0 ){decrementPregnancy();}

	incrementAge(); /* maybe better if dies of age before move */
	return 1;
}



/* main method for human intelligent behaivour based on its surrounding environment */
Human::ACTION Human::look(int* x, int *y)
{

	//track nearby empty cells for moving
	int free_count = 0;
    int empty_neighbors[4] = {0};
    
	//track nearby male ( used by female for reproduction)
	int male_count =0;
	int male_neighbors[4] = {0};

    // track nearby zombies
	int zombie_count =0;
	int zombie_neighbors[4] = {0};

	// save the entities around you
	Entity * entity_direction[4] = {_world->get(_x, _y-1),  // north
									_world->get(_x, _y+1),  // south
									_world->get(_x+1, _y),  // east
									_world->get(_x-1, _y)}; // west

    
   // analyze what is around you
	for(int i=0; i< 4; i++)
	{       	
		if(entity_direction[i] == NULL)
		{
			empty_neighbors[free_count] = i;
			free_count++;
		}
        
        // if entity is female, look nearby for male for reproduction
		if(entity_direction[i] != NULL 	
			&& maleCanReproduce(entity_direction[i])
			&& canBirth())
			// && _gender ==0
			// && entity_direction[i]->type() == HUMAN
		{ 
            male_neighbors[male_count] = i;
            male_count++;            
		}
		if(entity_direction[i] != NULL && entity_direction[i]->type() == ZOMBIE)
		{
			zombie_neighbors[zombie_count] = i;
			zombie_count++;
		}
	}
    
    
    /* decide what to do
    */
	int direction = 0;

	// attack if there is one zombie next to you
	if(zombie_count == 1 && hasGun()) 
	{
		direction = zombie_neighbors[_mt.genrand_int31() % zombie_count];
		getDirectionXY(direction, x,y);
		return Human::ATTACK;
    }
	
	// run if there is 2 or more zombies
	if((zombie_count >1 && free_count>0) || (zombie_count >= 1 && free_count >0 && !hasGun()) )
    {
        direction = empty_neighbors[_mt.genrand_int31() % free_count];
        getDirectionXY(direction, x, y);
        return Human::MOVE;
    }

	// no where to move, attack one of the zombies
	if(zombie_count >1 && free_count==0 && hasGun())
	{
		direction = zombie_neighbors[_mt.genrand_int31() % zombie_count];
		getDirectionXY(direction, x,y);
		return Human::ATTACK;		
	}
	

	// reproduce with nearby male
	if (male_count > 0)
    {
        direction = male_neighbors[_mt.genrand_int31() % male_count];
        getDirectionXY(direction, x, y);
        return Human::REPRODUCE;
    }
    
	// 50% of moving, 50% chance stay in current location
	if (free_count>0 && (_mt.genrand_int31() % 2 ==0))
    { 
        direction = empty_neighbors[_mt.genrand_int31() % free_count];
        getDirectionXY(direction, x, y);
        return Human::MOVE;
    }
    
	return Human::STAY;
} 



/* move to another cell in mesh (or to another mesh)
 *
 */
void Human::move(int x, int y){

    //remove myself from old position in world
	_world->remove(_x, _y);
	
    //update my new location
    _x = x;
    _y = y;
    
    // move myself to new position in world
    _world->put(x,y,this);
}


/* attack adjacent Zombie
 * success depends on your gun :)
 */
void Human::attack(int x, int y){
    
    Entity* target = _world->get(x, y);    
    
	if (target == NULL || (target != NULL && target->type() == HUMAN))
	{
		throw *(new ZWException("[Human::attack] Cannot eat NULL or Human"));
	}
    
	if(target->type() == ZOMBIE)
    {
        target->gotAttacked(_world);
    }
}

/* only move to an empty cell
 */
bool Human::okToMove(int x, int y)
{

	if (_world->get(x,y) == NULL)
    {
        return true;
    }
    else
    {
        return false;
    }
}
/* got attacked by Zombie
 */
void Human::gotAttacked( World* myworld)
{
    LOG(printf("[Human::gotAttacked()]H(%d,%d) got attacked\n",_x, _y));
	if(_world == NULL){
		return;   
	}
	_world->remove(_x,_y);
	_world->addExposed(_x,_y, _age, _gender);    
	_world->freeDeletedEntity(this); 
}

/*  Humans can birth if:
 *  - they are over age of 18 and under age of 40
 *  - if they are femail
 *  - if not already pregnant
 */
bool Human::canBirth()
{
    if(_gender != 0){return false;}   /* 0 == female */
	if(_birthRemaining <= 0) {return false;}
	if(isPregnant()){return false;}
	if(getAge()<(HUMAN_FEMALE_REPRO_MIN_AGE * TICKS_PER_YEAR) || getAge() > (HUMAN_FEMALE_REPRO_MAX_AGE * TICKS_PER_YEAR)) {return false;}
    return true;
}

/* countdown pregnancy until childbirth */
void Human::decrementPregnancy()
{
	// do nothing if invoked by Male
	if(_gender == 1)
    {
        throw *(new ZWException("[Human::decrementPregnancy] Do not invoke on Male"));
    }

	// only do something if invoked by pregnant female
    if(_gender == 0 && isPregnant()){
        --_pregnantTimeLeft;

		// time to birth new Baby
        if(_pregnantTimeLeft < 1) /* ==0 */
        {
            LOG(printf("[Human::decrementPregnancy] H@(%d,%d) birth new child\n", _x,_y));

			// randomize baby sex, gun
			int  babyGender = _mt.genrand_int31() % 2;
			bool babyHasGun = _mt.genrand_int31() % 2;

			// create and place baby in the same world as parent
			Human * newBaby = new Human(0, babyGender,_x,_y,_world, babyHasGun);
			_world->moveToFree(newBaby,_x,_y,1);          

			// reset mother pregnant flag
			_isPregnant = false;
        }
    }
}

/* returns true if female human is pregnant */
bool Human::isPregnant()
{
	if (_gender == 1)
	{		
		throw *(new ZWException("[Human::isPregnant] Cannot invoke on Male! "));
	}
	return _isPregnant;
}

/* used by female to reproduce */
void Human::reproduce(int x, int y)
{
	if(_gender == 1)
	{
		throw *(new ZWException("[Human::reproduce] Cannot invoke on Male! "));
	}

	Entity* partner = _world->get(x,y);
	int partnerAge = partner->getAge();

	if (maleCanReproduce(partner) && canBirth())
	{
		_birthRemaining -= 1; 
		_pregnantTimeLeft = HUMAN_FEMALE_PREGNANCY_TICKS;  /* Global_Const */
		_isPregnant = true;
	}
}
/* checks if a male can reproduce with female */
bool Human::maleCanReproduce(Entity * male)
{
	return (male->type() == HUMAN
			&& male->getGender() == 1 
			&& male->getAge() > (HUMAN_MALE_REPRO_MIN_AGE * TICKS_PER_YEAR) 
			&& male->getAge() < (HUMAN_MALE_REPRO_MAX_AGE * TICKS_PER_YEAR));
}

/* used for testing only */
void Human::setPregnant(int pregTimeleft)
{
	_pregnantTimeLeft = pregTimeleft;
}

/* used for testing only */
int Human::getPregnantState()
{
	LOG(printf("[Human::getPregnantState]H@(%d,%d)_prgTimeLeft=%d birthRem=%d age=%d\n", _x,_y, _pregnantTimeLeft, _birthRemaining, getAge()));
	return _pregnantTimeLeft;
}

/* returns true if a human has a gun */
bool Human::hasGun()
{
	return _has_gun;
}
