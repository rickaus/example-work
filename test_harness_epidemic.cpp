/* C++
 *
 * Test harness for testing Human move, attack, birth, death, 
 * reproduction and state transition behaviour.
 *
 */

#include <iostream>
#include <cstdlib>
#include <memory>
#include <new>
#include <string>
#include "flags.h"
#include <limits.h>
#include <math.h>

#include <omp.h>
#include <mpi.h>

#include "World.h"
#include "Clock.h"
#include "Communicator.h"
#include "ZWException.h"
#include "Zombie.h"
#include "Human.h"
#include "Exposed.h"
#include "mtrnd.h"          /* Random Nummber Generator */
#include "Utils.h"
#define DEFAULT_TIME_SCALE (60 * 60)

using namespace std;

int main(int argc, char** argv)
{
	int total_size = 0;
	int my_rank = 0;
	int worldX = 100;
	int worldY = 150;
	int totalworldX = 1500;
	int totalworldY = 1000;

	try
	{
		// mpi initialization
		CALL_MPI(MPI_Init(&argc, &argv));

		CALL_MPI(MPI_Comm_size(MPI_COMM_WORLD, &total_size));
		CALL_MPI(MPI_Comm_rank(MPI_COMM_WORLD, &my_rank));

		cout << "WELCOME TO ZOMBIE LAND" << endl;
		cout << "my rank " << my_rank << " total " << total_size << endl;

        
        // create world object for this node
		World* world = new World(worldX, worldY, my_rank);
		
        // create communicator object for this node's world
		Communicator* comm = new Communicator(total_size, my_rank, worldX, worldY, totalworldX, totalworldY, world);
        
        int tempX, tempY;

		
/*----------------------------------------------------------------------*/

		if (my_rank == 0)
		{            
            printf("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@\n");
            printf("Female pregnant spawns new baby in 2 ticks \n");
            printf("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@\n");
			world->addHuman(3, 3, 10, 0, true);  //female
            world->addHuman(3, 4, 10, 1, true);  //male
            Entity *female = world->get(3,3);
            world->tick();
            female->activate();
            tempX = female->getX();
            tempY = female->getY();
		}
        if (my_rank == 1)
        {
            
            printf("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@\n");
            printf(" IGNORE THIS TEST - BLANK SPACE \n");
            printf("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@\n");
			world->addHuman(3, 3, 10, 0, true);  //female
            world->addHuman(3, 4, 10, 0, true);  //male
            Entity *female = world->get(3,3);
            world->tick();
            female->activate();
            tempX = female->getX();
            tempY = female->getY();
        }
		comm->synchronizeData();
		world->printWorld(my_rank);
        
        // cleanup test data
        if(my_rank==0)
        {
            world->remove(tempX,tempY);
            world->remove(3,4);
        }
        if(my_rank ==1)
        {
            world->remove(tempX,tempY);
            world->remove(3,4);
        }
        

/*----------------------------------------------------------------------*/
		if (my_rank == 0)
		{            
            printf("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@\n");
            printf("Female Human reproduce with South Male Human \n");
			printf("Female Human spawns new baby in 2 ticks \n");
            printf("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@\n");
			world->addHuman(3, 3, 20*365, 0, true);  //female
            world->addHuman(3, 4, 20*365, 1, true);  //male
            Human *female = static_cast<Human*> (world->get(3,3));
            int i = female->getPregnantState();
			world->tick();			// 1
            female->activate();
			i = female->getPregnantState();
			world->tick();			// 2 uncomment to see child birth
            female->activate();
			i = female->getPregnantState();
            tempX = female->getX();
            tempY = female->getY();
		}
        if (my_rank == 1)
        {
            
            printf("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@\n");
            printf("Female do not reproduce with South Female \n");
            printf("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@\n");
			world->addHuman(3, 3, 10, 0, true);  //female
            world->addHuman(3, 4, 10, 0, true);  //male
            Entity *female = world->get(3,3);
            world->tick();
            female->activate();
            tempX = female->getX();
            tempY = female->getY();
        }
		comm->synchronizeData();
		world->printWorld(my_rank);
        
        // cleanup test data
        if(my_rank==0)
        {
            world->remove(tempX,tempY);
            world->remove(3,4);
        }
        if(my_rank ==1)
        {
            world->remove(tempX,tempY);
            world->remove(3,4);
        }
        
/*----------------------------------------------------------------------*/
		if (my_rank == 0)
		{
            
            printf("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@\n");
            printf("Zombie ignore South Exposed entity \n");
            printf("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@\n");
			world->addZombie(3, 2, 10, 0);
            world->addExposed(3, 3, 10, 0);
            Entity *e = world->get(3,2);
            world->tick();
            e->activate();
            tempX = e->getX();
            tempY = e->getY();
		}
        if (my_rank == 1)
        {
            
            printf("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@\n");
            printf("Zombie ignores North Exposed entity \n");
            printf("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@\n");
            world->addZombie(3, 3, 10, 0);
            world->addExposed(3, 2, 10, 0);
            Entity *e = world->get(3,3);
            world->tick();
            e->activate();
            tempX = e->getX();
            tempY = e->getY();
        }
		comm->synchronizeData();
		world->printWorld(my_rank);
        
        // cleanup test data
        if(my_rank==0)
        {
            world->remove(tempX,tempY);
            world->remove(3,3);
        }
        if(my_rank ==1)
        {
            world->remove(tempX,tempY);
            world->remove(3,2);
        }
       
/*----------------------------------------------------------------------*/
        
        if (my_rank ==0)
        {
            printf("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@\n");
            printf(" Exposed do not age \n");
            printf("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@\n");
            world->addExposed(3, 3, 29199, 0);   //does not die at tick #20
            Entity *e = world->get(3,3);
            world->tick();   // 1
            e->activate();
            world->tick();   // 2   should die here
            e->activate();
            //world->tick();   // 3 become zombie
            //e->activate();
            tempX = e->getX();
            tempY = e->getY();
        }
        if (my_rank ==1)
        {
            printf("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@\n");
            printf(" Human dies from old age after 3 ticks \n");
            printf("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@\n");
            world->addHuman(4, 3, 29197, 0, true);   //dies at tick #80
            Entity *e = world->get(4,3);
            world->tick();  // 1
            e->activate();
            world->tick();  // 2
            e->activate();
            world->tick();  // 3
            e->activate();
            tempX = e->getX();
            tempY = e->getY();
            
        }
 		comm->synchronizeData();
		world->printWorld(my_rank);
        
        // cleanup test data
        if(my_rank==0)
        {
            world->remove(tempX,tempY);
            
        }
        if(my_rank ==1)
        {
            //world->remove(tempX,tempY);
        }
       
/*----------------------------------------------------------------------*/
        
        
        if (my_rank ==0)
        {
            printf("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@\n");
            printf(" Zombie dies from old age after 3 ticks \n");
            printf("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@\n");
            world->addZombie(3, 3, 18247, 0);   //dies at tick #20
            Entity *e = world->get(3,3);
            world->tick();   // 1
            e->activate();
            world->tick();   // 2   dies here
            e->activate();
            world->tick();   // 3
            e->activate();
            tempX = e->getX();
            tempY = e->getY();
        }
        if (my_rank ==1)
        {
            printf("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@\n");
            printf(" Human dies from old age after 3 ticks \n");
            printf("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@\n");
            world->addHuman(4, 3, 29197, 0, true);   //dies at tick #80
            Entity *e = world->get(4,3);
            world->tick();  // 1
            e->activate();
            world->tick();  // 2
            e->activate();
            world->tick();  // 3
            e->activate();
            tempX = e->getX();
            tempY = e->getY();
            
        }
 		comm->synchronizeData();
		world->printWorld(my_rank);
        
        // cleanup test data
        if(my_rank==0)
        {
           // world->remove(tempX,tempY);

        }
        if(my_rank ==1)
        {
           // world->remove(tempX,tempY);
        }
        
/*----------------------------------------------------------------------*/
        
        if (my_rank ==0)
        {
            printf("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@\n");
            printf(" Human detect attack East Zombie \n");
            printf("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@\n");
            world->addHuman(3, 3, 30, 0, true);
            world->addZombie(4, 3, 10, 0);
            world->tick();
            Entity *e = world->get(3,3);
            e->activate();
        }
        if (my_rank ==1)
        {
            printf("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@\n");
            printf(" Human detect attack West Zombie \n");
            printf("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@\n");
            world->addHuman(3, 3, 30, 0, true);
            world->addZombie(2, 3, 10, 0);
            world->tick();
            Entity *e = world->get(3,3);
            e->activate();
            
        }
 		comm->synchronizeData();
		world->printWorld(my_rank);
        
        // cleanup test data
        if(my_rank==0)
        {
            world->remove(3,3);
            //world->remove(4,3);
        }
        if(my_rank ==1)
        {
            world->remove(3,3);
            //world->remove(2,3);
        }
/*----------------------------------------------------------------------*/
        
        if (my_rank ==0)
        {
            printf("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@\n");
            printf(" Human detect attack North Zombie \n");
            printf("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@\n");
            world->addHuman(3, 3, 30, 0, true);
            world->addZombie(3, 2, 10, 0);
            world->tick();
            Entity *e = world->get(3,3);
            e->activate();
        }
        if (my_rank ==1)
        {
            printf("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@\n");
            printf(" Human detect attack South Zombie \n");
            printf("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@\n");
            world->addHuman(3, 3, 30, 0, true);
            world->addZombie(3, 4, 10, 0);
            world->tick();
            Entity *e = world->get(3,3);
            e->activate();
            
        }
 		comm->synchronizeData();
		world->printWorld(my_rank);
        
        // cleanup test data
        if(my_rank==0)
        {
            world->remove(3,3);
            //world->remove(3,2);
        }
        if(my_rank ==1)
        {
            world->remove(3,3);
            //world->remove(3,4);
        }
/*----------------------------------------------------------------------*/ 
        
        //int tempX, tempY;
        
        if (my_rank ==0)
        {
            printf("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@\n");
            printf(" Test Human move \n");
            printf("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@\n");
            world->addHuman(3, 3, 30, 0, true);
            world->tick();
            Entity *e = world->get(3,3);
            e->activate();
            tempX = e->getX();
            tempY = e->getY();
        }
        if (my_rank ==1)
        {
            printf("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@\n");
            printf(" Test Human Move\n");
            printf("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@\n");
            world->addHuman(3, 3, 30, 0, true);
            world->tick();
            Entity *e = world->get(3,3);
            e->activate();
            tempX = e->getX();
            tempY = e->getY();
        }
 		comm->synchronizeData();
		world->printWorld(my_rank);
        
        // cleanup test data
        if(my_rank==0)
        {
            world->remove(tempX,tempY);
        }
        if(my_rank ==1)
        {
            world->remove(tempX,tempY);
        }       
          
/*----------------------------------------------------------------------*/


        if (my_rank ==0)
        {
            printf("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@\n");
            printf("Add Exposed to 3,3 \n");
            printf("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@\n");
            world->addExposed(3, 3, 10, 0);
        }
        if (my_rank ==1)
        {
            printf("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@\n");
            printf("Exposed becomes Zombie after 3 ticks \n");
            printf("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@\n");
            Exposed * e = new Exposed(10,0,2,3, world);
            printf("[test harness](2,3) type=%d \n", e->type());
            world->put(2,3,e);
            world->tick(); //1
            e->activate();
            world->tick(); //2
            e->activate();
            printf("[test harness](2,3) type=%d \n", e->type());
            world->tick(); //3 Exposed becomes Zombie                      
            e->activate();
            Entity * f = world->get(2,3);
            printf("[test harness](2,3) type=%d \n", f->type());

        }
 		comm->synchronizeData();
		world->printWorld(my_rank);
        
        // cleanup test data
        if(my_rank==0)
        {
            world->remove(3,3);
        }
        if(my_rank ==1)
        {
            world->remove(2,3);
        }
        
       
/*----------------------------------------------------------------------*/         

 
		if (my_rank == 0)
		{            
            printf("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@\n");
            printf("Zombie only attack once per tick cycle.\n");
            printf("Zombie detect attack South Human\n");
            printf("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@\n");
			world->addZombie(2, 2, 10, 0);
            world->addHuman(2, 3, 30, 0,true);
            Entity *e = world->get(2,2);
            world->tick();
            e->activate();            
            e->activate();
            
            
		}
        if (my_rank == 1)
        {
            printf("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@\n");
            printf("Zombie attack North Human.\n");
            printf("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@\n");
            world->addZombie(2, 2, 10, 0);
            world->addHuman(2, 1, 30, 0, true);            
            Entity *e = world->get(2,2);
            world->tick();
            e->activate();
        }
		comm->synchronizeData();
		world->printWorld(my_rank);
       
        // cleanup test data
        if(my_rank==0)
        {
            world->remove(2,2);
            world->remove(2,3);
        }
        if(my_rank ==1)
        {
            world->remove(2,2);
            world->remove(2,1);
        }
        
/*----------------------------------------------------------------------*/
        
        
        
        world->tick();
		if (my_rank == 0)
		{
            //world->remove(2,3);
            printf("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@\n");
            printf("Zombie detect attack West Human\n");
            printf("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@\n");
			world->addZombie(2, 2, 10, 0);
            world->addHuman(1, 2, 30, 0,true);
            Entity *e = world->get(2,2);
            world->tick();
            e->activate();           
		}
        if (my_rank == 1)
        {
            //world->remove(2,1);
            printf("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@\n");
            printf("Zombie attack East Human.\n");
            printf("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@\n");
            world->addZombie(2, 2, 10, 0);
            world->addHuman(3, 2, 30, 0, true);
            Entity *e = world->get(2,2);
            world->tick();
            e->activate();
        }
		comm->synchronizeData();
		world->printWorld(my_rank);
        
        // cleanup test data
        if(my_rank==0)
        {
            world->remove(2,2);
            world->remove(1,2);
        }
        if(my_rank ==1)
        {
            world->remove(2,2);
            world->remove(3,2);
        }

/*----------------------------------------------------------------------*/
        
        
        /*
		for(int z =0; z < 2 ; z++){
			
            #pragma omp parallel for num_threads(2)
			for(int i= 1; i <= worldX; i++){
				world->lockColumn(i);
				for(int j=1; j <= worldY; j++){

					Entity* ent = world->get(i,j);
					if(ent != NULL){
						ent->activate();
					}
				}
				world->unlockColumn(i);
			}
			
			
			comm->synchronizeData();
			world->tick();
			world->printWorld(my_rank);
			
		}*/
        

		CALL_MPI(MPI_Finalize());
	}catch(ZWException &ex){
		printf("error exception %s \n",ex.what());
	}
}
