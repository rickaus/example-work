EXAMPLE WORK
====

** Please contact me for complete project source code.

[Link to files mentioned below.](https://bitbucket.org/rickaus/example-work/src "code files")

***

[1] Real Time Telephone Analytics (Java)
----

A real-time stream processing application that analyses phone calls to qualify users to promotions and generate user analytics.

`CallStatisticsBolt.java`

A backend component of the stream platform that provides analytics data to the gui: 

*   Top Callers,
*   Top Called Area Codes,
*   Total Calls, Total Minutes, Average Minutes Per Call.

For scalability,  multiple instances of CallStatisticsBolt are run in parallel, and their outputs are merged by another downstream component.

![GUI][100]

***

[2] Epidemic Modelling (C++)
----

A multicore and multi threaded simulation to model disease infection of 250,000 humans over 10 years. The program was run on an iDataplex x86 machine.

`Human.cpp`

Models Human behaviour in an epidemic model. Humans have their own intelligence to move, attack, reproduce and fear.

`test_harness_epidemic.cpp`

Test harness for testing Human move, attack, birth, death, reproduction and state transition behaviour.


![Simulation Results][101]

    Green - Healthy Male
    Pink - Healthy Female
    Yellow - Infected Human
    Red - Contagious Human
    This extract is less than 5% of the full-sized simulation

***

[3] Approximate Spelling Correction (Python)
----
coming soon

***
[100]: https://bitbucket.org/rickaus/example-work/raw/175b127fa4ead6bc181f5208e1128ba50b175d3f/real_time_analytics.png
[101]: https://bitbucket.org/rickaus/example-work/raw/175b127fa4ead6bc181f5208e1128ba50b175d3f/epidemic.png